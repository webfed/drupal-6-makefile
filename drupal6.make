core = 6.x
api = 2


;core =========================================================================

projects[pressflow][type] = "core"
projects[pressflow][download][type] = "get"
projects[pressflow][download][url] = "http://files.aegir.cc/core/pressflow-6.36.1.tar.gz"


; Modules =====================================================================

projects[admin][version] = "2.0"

projects[admin_menu][version] = "1.9"

projects[advanced_help][version] = "1.2"

projects[audiofield][version] = "1.2"

projects[autoload][version] = "2.1"

projects[availability_calendars][version] = "2.2"

projects[backup_migrate][version] = "2.8"

projects[backup_migrate_files][version] = "1.x-dev"

projects[calendar][version] = "2.4"

projects[cck][version] = "2.10"

projects[colorbox][version] = "1.4"

projects[content_access][version] = "1.2"

;projects[content_multigroup][version] = "1.0"

projects[content_taxonomy][version] = "1.0-rc2"

projects[context][version] = "3.0"

projects[css3pie][version] = "1.0-beta2"

projects[ctools][version] = "1.13"

projects[cufon][version] = "1.0-beta4"

projects[cufon][download][type] = "git"
projects[cufon][download][url] = "https://webfed@bitbucket.org/webfed/cufon-module-d6.git"
projects[cufon][directory_name] = "cufon"
projects[cufon][type] = "module"

projects[date][version] = "2.10"

projects[dbtuner][version] = "1.0-alpha2"

projects[devel][version] = "1.26"

; ERROR projects[dtools]

projects[email][version] = "1.4"

projects[emfield][version] = "2.6"

;projects[eu-cookie-compliance][version] = "1.8"

projects[event][version] = "2.x-dev"

projects[event_views][version] = "2.4"

projects[extlink][version] = "1.17"

projects[fasttoggle][version] = "1.3"

projects[fblikebutton][version] = "1.6"

projects[fckeditor][version] = "1.3"

projects[filefield][version] = "3.13"

projects[flashnode][version] = "3.1"

projects[forward][version] = "1.21"

projects[globalredirect][version] = "1.5"

projects[google_analytics][version] = "3.6"

projects[google_fonts][version] = "1.7"

projects[html_export][version] = "1.0"

projects[honeypot][version] = "1.18"

projects[i18n][version] = "1.10"

projects[i18nviews][version] = "2.0"

projects[ie_css_optimizer][version] = "1.1"

projects[image_fupload][version] = "3.0-rc2"

projects[image_resize_filter][version] = "1.14"

projects[imageapi][version] = "1.10"

projects[imagecache][version] = "2.0-rc1"

projects[imagecache_actions][version] = "1.9"

projects[imagecrop][version] = "1.2"

project[imagefield][version] = "3.11"

projects[imce][version] = "2.7"

projects[imce_wysiwyg][version] = "1.1"

projects[jcarousel][version] = "2.6"

projects[jq][version] = "1.2"

projects[jq_eyecandy][version] = "1.x-dev"

projects[jquery_plugin][version] = "1.10"

projects[jquery_ui][version] = "1.5"

projects[jquery_ui][download][type] = "git"
projects[jquery_ui][download][url] = "https://webfed@bitbucket.org/webfed/jquery_ui.git"
projects[jquery_ui][directory_name] = "jquery_ui"
projects[jquery_ui][type] = "module"

projects[jquery_update][version] = "2.0-alpha1"

projects[languageicons][version] = "2.1"

projects[libraries][version] = "1.0"

projects[lightbox2][version] = "1.9"

projects[link][version] = "2.11"

projects[linkit][version] = "1.12"

projects[linkit_panel_pages][version] = "1.0"

projects[login_destination][version] = "2.12"

projects[logintoboggan][version] = "1.11"

projects[masquerade][version] = "1.9"

projects[media_vimeo][version] = "1.1"

projects[media_youtube][version] = "1.3"

projects[menu_per_role][version] = "1.9"

projects[mimemail][version] = "1.2"

projects[mobileplugin][version] = "2.0"

projects[module_filter][version] = "1.7"

projects[nice_menus][version] = "2.1"

projects[node_export][version] = "3.4"

projects[nodeformsettings][version] = "2.0"

projects[nodequeue][version] = "2.11"

projects[nodereference_url][version] = "1.3"

projects[nodereferrer][version] = "1.0-rc2"

projects[nodewords][version] = "1.14"

projects[oauth][version] = "3.0-beta4"

projects[page_title][version] = "2.7"

projects[panels][download][type] = "git"
projects[panels][download][url] = "https://webfed@bitbucket.org/webfed/panels.git"
projects[panels][directory_name] = "panels"
projects[panels][type] = "module"

projects[parallel][version] = "1.0-beta2"

projects[path_redirect][version] = "1.0-rc2"

projects[pathauto][version] = "1.6"

projects[pathologic][version] = "3.4"

projects[phone][version] = "2.14"

projects[print][version] = "1.14"

projects[private_upload][version] = "1.0-rc3"

projects[robotstxt][version] = "1.3"

projects[scheduler][version] = "1.10"

projects[semanticviews][version] = "1.1"

projects[service_links][version] = "2.1"

projects[site_map][version] = "2.2"

projects[simplenews][version] = "1.5"

projects[sitedoc][version] = "1.5"

projects[skinr][version] = "1.6"

projects[smtp][type] = "module"
projects[smtp][download][type] = "git"
projects[smtp][download][url] = "https://webfed@bitbucket.org/webfed/smtp.git"
projects[smtp][subdir] = "modules"

projects[swftools][version] = "2.5"

projects[taxonomy_breadcrumb][version] = "1.1"

projects[taxonomy_manager][version] = "2.3"

projects[taxonomy_menu][version] = "2.9"

projects[theme_editor][version] = "1.4"

projects[token][version] = "1.19"

projects[total_control][version] = "1.2"

projects[translation_helpers][version] = "1.0"

projects[translation_overview][version] = "2.4"

projects[transliteration][version] = "3.1"

projects[twitter][version] = "5.1"

projects[uuid][version] = "1.0-rc2"

projects[vertical_tabs][version] = "1.0-rc2"

projects[videojs][version] = "1.9"

projects[views][version] = "2.18"

projects[views_accordion][version] = "1.5"

projects[views_attach][version] = "2.2"

projects[views_bulk_operations][version] = "1.16"

projects[views_gallery][version] = "1.2"

projects[views_slideshow][version] = "2.4"

projects[viewscarousel][version] = "2.x-dev"

projects[webform][version] = "3.23"

projects[webform_validation][version] = "1.5"

projects[wysiwyg][version] = "2.4"

projects[xmlsitemap][version] = "1.2"



; Features =====================================================================



; Patches =====================================================================


;Todo =====================================================================


; Themes =====================================================================

projects[acquia_marina][version] = "2.0"

projects[acquia_slate][type] = theme
projects[acquia_slate][version] = "2.0"

projects[rootcandy][version] = "1.9"
projects[fusion][version] = "1.13"

projects[rubik][version] = "3.0-beta1"

projects[tao][version] = "3.1"

projects[zen][type] = theme
projects[zen][version] = "1.2"
projects[zen][download][type] = "get"
projects[zen][download][url] = "http://ftp.drupal.org/files/projects/zen-6.x-1.2.zip"


; Libraries =====================================================================

libraries[ckeditor][download][type] = "get"
libraries[ckeditor][download][url] = "http://download.cksource.com/CKEditor/CKEditor/CKEditor%203.4.2/ckeditor_3.4.2.zip"
libraries[ckeditor][directory_name] = "ckeditor"
libraries[ckeditor][type] = "library"

libraries[colorbox][download][type] = "git"
libraries[colorbox][download][url] = "https://webfed@bitbucket.org/webfed/colorbox-library-d6.git"
libraries[colorbox][directory_name] = "colorbox"
libraries[colorbox][type] = "library"

libraries[cufon][download][type] = "get"
libraries[cufon][download][url] = "http://cdnjs.cloudflare.com/ajax/libs/cufon/1.09i/cufon-yui.js"
libraries[cufon][directory_name] = "cufon"
libraries[cufon][type] = "library"


libraries[cufon-fonts][download][type] = "git"
libraries[cufon-fonts][download][url] = "https://webfed@bitbucket.org/webfed/cufon-fonts.git"
libraries[cufon-fonts][directory_name] = "cufon-fonts"
libraries[cufon-fonts][type] = "library"


libraries[jqueryui][download][type] = "get"
libraries[jqueryui][download][url] = "http://jquery-ui.googlecode.com/files/jquery.ui-1.6.zip"
libraries[jqueryui][directory_name] = "jquery.ui"
libraries[jqueryui][type] = "library"


libraries[PIE][download][type] = "get"
libraries[PIE][download][url] = "https://github.com/lojjic/PIE/archive/master.zip"
libraries[PIE][directory_name] = "PIE"
libraries[PIE][type] = "library"

libraries[video-js][download][type] = "git"
libraries[video-js][download][url] = "https://github.com/videojs/video.js.git"
libraries[video-js][directory_name] = "video-js"
libraries[video-js][type] = "library"
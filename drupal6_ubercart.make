core = 6.x
api = 2

includes[drupal6.make] = "https://bitbucket.org/webfed/drupal-6-makefile/raw/1b19c6cc83c86737a1c357cfce2772ad2f1d4943/drupal6.make"

; Modules =====================================================================

projects[modalframe][version] = "1.7"
projects[ubercart][version] = "2.13"
projects[uc_ajax_cart][version] = "2.0"
projects[uc_coupon][version] = "1.7"
projects[uc_discounts_alt][version] = "2.4"
projects[uc_ideal_lite][version] = "1.4"
projects[uc_out_of_stock][version] = "1.7"
projects[uc_simple_quote][version] = "1.0"
projects[uc_termsofservice][version] = "1.2"
projects[uc_vat][version] = "1.2"
projects[uc_views][version] = "3.3"
projects[uc_wishlist][version] = "1.1"


; Features =====================================================================



; Patches =====================================================================

projects[uc_ajax_cart][patch][] = "https://drupal.org/files/issues/1213574-send-clicked-button-js.patch"
;projects[uc_taxes][patch][] = "https://drupal.org/files/issues/uc_taxes.module_4.patch"
projects[uc_vat][patch][] = "https://drupal.org/files/issues/tax_amount_fix_attempt-1170992-21.patch"




;Todo =====================================================================


; Themes =====================================================================




; Libraries =====================================================================